var PageLinks = $$('a').map(function(link) {
    return link.getAttribute("href");
}).then(function(links) {
    links.forEach(function(link) {
        browser.ignoreSynchronization = true;
		browser.get(link);
        expect(browser.getCurrentUrl()).not.toContain('/Error/');
		browser.ignoreSynchronization = false;
    });
});
module.exports = PageLinks;