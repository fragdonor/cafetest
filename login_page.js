var LoginPage = function() {
  var EC = protractor.ExpectedConditions;
  this.url = "http://cafetownsend-angular-rails.herokuapp.com/login";
  this.usernameField = element(by.model("user.name"));
  this.passwordField = element(by.model("user.password"));
  this.loginButton = element(by.buttonText("Login"));

  this.login = function(user, pw) {
    loginButton = element(by.buttonText("Login"));
	this.usernameField.sendKeys(user);
	this.passwordField.sendKeys(pw);
	browser.wait(EC.elementToBeClickable(loginButton, 6000));
	loginButton.click();
  };
};

module.exports = LoginPage;