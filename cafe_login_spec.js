var LoginPage = require("./login_page");
var EmployeesPage = require("./employees_page");

describe("Test Cafe login page", function(){
	beforeEach(function() {
			loginPage = new LoginPage();
			browser.get(loginPage.url);
	});

	it("To test empty input fields and check login is not working", function(){
		loginPage.usernameField.click();
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(loginPage.loginButton)).toEqual(true);
		
	});
	it("To test correct username and empty password and check login is not working", function(){
		loginPage.login("Luke", "");
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(by.buttonText("Login"))).toEqual(true);
	});
	it("To test empty username and correct password and check login is not working", function(){
		loginPage.login("", "Skywalker");
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(by.buttonText("Login"))).toEqual(true);
	});
	it("To test correct username lowcase and correct password and check login is not working", function(){
		loginPage.login("luke", "Skywalker");
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(by.buttonText("Login"))).toEqual(true);
	});
	it("To test correct username and correct password lowcase and check login is not working", function(){
		loginPage.login("Luke", "skywalker");
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(by.buttonText("Login"))).toEqual(true);
	});
	it("To test correct username lowcase and correct password lowcase and check login is not working", function(){
		loginPage.login("luke", "skywalker");
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(by.buttonText("Login"))).toEqual(true);
	});
});
