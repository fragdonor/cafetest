var countBefore=-1;
var countAfter=-1;
var DeleteFunction = function() {
    this.deleteAll = function() {
        element.all(by.css("#employee-list li")).each(function (elem) {
            var countBefore = element.all(by.repeater('employee in employees')).then(function(elements){
                return elements.length;
            }).then(function(){
                var firstElem = element.all(by.repeater('employee in employees')).first();
                firstElem.click().then(function () {
                    //If you want perform some operation after click, you can do it here.
                    browser.sleep(100);}).then(function(){
                        element(by.css("#bDelete")).click();
                        browser.sleep(100);}).then(function(){
                            browser.switchTo().alert().accept();
                            var countAfter = element.all(by.repeater('employee in employees')).then(function(elements){
                                return elements.length;
                            });
                            while (countAfter===countBefore){console.log("Waiting for "+countAfter);}
                        });
                    });
                })

    };
    this.deleteEntriesFromList = function(x) {
        element.all(by.css("#employee-list li")).each(function (elem) {
            elem.getText().then(function(text){console.log("Searched value - "+x+"  and found value - "+text);
                    countBefore = element.all(by.css("#employee-list li")).then(function(elements){
                        console.log("Count before - " + elements.length);
                        return elements.length;}).then(function(){
                        elem.click().then(function () {
                            //If you want perform some operation after click, you can do it here.
                            browser.sleep(100);}).then(function(){
                            element(by.css("#bDelete")).click();
                            browser.sleep(100);}).then(function(){
                            browser.switchTo().alert().accept();
                            countAfter = element.all(by.css("#employee-list li")).then(function(elements){
                                return elements.length;
                            });
                            while (countAfter===countBefore){console.log("Waiting for "+countAfter);}
                        });
                    });
            });



        })

    };
};

module.exports = DeleteFunction;