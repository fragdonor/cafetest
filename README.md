# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This project tests http://cafetownsend-angular-rails.herokuapp.com/ page with protractor

### How do I get set up? ###

1. Install node.js 6xxx   and chrome
2. Console: npm install -g protractor
3. Install JDK 8
4. Console: webdriver-manager update
5. Console: webdriver-manager start
6. Clone this repository
7. Go to the repository directory
8. New console: protractor cafe_spec.js

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact