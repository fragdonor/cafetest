var LoginPage = require("./login_page");
var EmployeesPage = require("./employees_page");
var username = "Luke";
var pw = "Skywalker";

describe("Test Cafe employees page", function(){
	beforeEach(function() {
		loginPage = new LoginPage();
		employeesPage = new EmployeesPage();
		browser.get(loginPage.url);
		loginPage.login(username, pw);
	});
	it("To test greeting", function(){
		expect(employeesPage.greeting.getText()).toEqual('Hello '+username);
	});
	it("To test url", function(){
		expect(browser.getCurrentUrl()).toEqual(employeesPage.url);
	});
	it("To test Logout function", function(){
		employeesPage.logoutButton.click();
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(loginButton)).toEqual(true);
		expect(loginPage.usernameField.getAttribute('value')).toEqual("");
		expect(loginPage.passwordField.getAttribute('value')).toEqual("");
	});
});