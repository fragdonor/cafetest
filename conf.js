// An example configuration file.
exports.config = {
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',

  // Spec patterns are relative to the current working directory when
  // protractor is called.
  specs: ['cafe_login_spec.js',
      'cafe_employees_spec.js',
      'cafe_edit_spec.js',
      'cafe_create_spec.js',
      'cafe_update_spec.js'
      ],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
	showColors:true,
    defaultTimeoutInterval: 300000
	
  }
};
