var LoginPage = require("./login_page");
var EmployeesPage = require("./employees_page");
var CreatePage = require("./create_page");
var DeleteFunction = require("./delete_function");

var username = "Luke";
var pw = "Skywalker";
var firstname = "first";
var lastname = "last";
var startdate = "2050-10-10";
var email = "email@google.com";

describe("Test Cafe login page", function(){
	beforeEach(function() {
		loginPage = new LoginPage();
		employeesPage = new EmployeesPage();
		createPage = new CreatePage();
        deleteFunction = new DeleteFunction();
		browser.get(loginPage.url);
		loginPage.login(username, pw);
        //deleteFunction.deleteAll();
        deleteFunction.deleteEntriesFromList(firstname+" "+lastname);
        browser.sleep(500);
		employeesPage.createButton.click();
	});
    it("To test new element is created", function(){
        createPage.firstName.sendKeys(firstname);
        createPage.lastName.sendKeys(lastname);
        createPage.startDate.sendKeys(startdate);
        createPage.email.sendKeys("email@gmail.com");
        var xpathExp = "//li[contains(.,'"+firstname+' '+lastname+"')]";
        createPage.addButton.click();
        browser.sleep(1000);
        expect(employeesPage.countEntries(firstname+" "+lastname)).toEqual(1);
        expect(browser.getCurrentUrl()).toEqual(employeesPage.url);
        expect(element(by.xpath(xpathExp)).isDisplayed()).toEqual(true);
    });
	it("To test greeting", function(){
		expect(createPage.greeting.getText()).toEqual('Hello '+username);
	});
	it("To test url", function(){
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test logout", function(){
		createPage.logoutButton.click();
		expect(browser.getCurrentUrl()).toEqual(loginPage.url);
		expect(browser.isElementPresent(loginButton)).toEqual(true);
		expect(loginPage.usernameField.getAttribute('value')).toEqual("");
		expect(loginPage.passwordField.getAttribute('value')).toEqual("");
	});
	it("To test cancel button", function(){
		createPage.cancelButton.click();
		expect(browser.getCurrentUrl()).toEqual(employeesPage.url);
	});
	it("To test First name required", function(){
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys(email);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Last name required", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys(email);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Start date required", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.email.sendKeys(email);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Email date required", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test First name with numbers is not allowed", function(){
		createPage.firstName.sendKeys("f1irst");
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test First name with special symbols is not allowed", function(){
		createPage.firstName.sendKeys("first!");
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Last name with numbers is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys("last1");
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test First name with special symbols is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys("last%");
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Date with military type is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys("10102017");
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
        expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Date with text is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys("10t1sd017");
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
        expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Date with wrong numbers is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys("12345678912345678");
		createPage.email.sendKeys(email);
        browser.sleep(100);
		createPage.addButton.click();
        expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Email without @ is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys("email");
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Email without ending is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys("email@gmail");
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Email with numbers in front is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys("1email@gmail.com");
        browser.sleep(100);
		createPage.addButton.click();
		expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	it("To test Email with wrong ending is not allowed", function(){
		createPage.firstName.sendKeys(firstname);
		createPage.lastName.sendKeys(lastname);
		createPage.startDate.sendKeys(startdate);
		createPage.email.sendKeys("email@gmail.c");
        browser.sleep(100);
		createPage.addButton.click();
        expect(browser.getCurrentUrl()).toEqual(createPage.url);
	});
	
});
