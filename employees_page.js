var EmployeesPage = function() {
  this.url = "http://cafetownsend-angular-rails.herokuapp.com/employees";
  this.greeting = element(by.id("greetings"));
  this.createButton = element(by.id("bAdd"));
  this.editButton = element(by.id("bEdit"));
  this.deleteButton = element(by.id("bDelete"));
  this.logoutButton = element(by.css("body > div > header > div > p.main-button"));

  this.countEntries = function(x) {
        element.all(by.css("#employee-list [contains(.,'"+x+"')]")).then(function (elements) {
            return elements.length;
        });
  };
};

module.exports = EmployeesPage;