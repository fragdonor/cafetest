var CreatePage = function() {
  this.url = "http://cafetownsend-angular-rails.herokuapp.com/employees/new";
  this.greeting = element(by.id("greetings"));
  this.cancelButton = element(by.className("subButton bCancel"));
  this.logoutButton = element(by.css("body > div > header > div > p.main-button"));
  this.addButton = element(by.buttonText("Add"));
  this.firstName = element(by.model("selectedEmployee.firstName"));
  this.lastName = element(by.model("selectedEmployee.lastName"));
  this.startDate = element(by.model("selectedEmployee.startDate"));
  this.email = element.all(by.model("selectedEmployee.email"));

  this.clearFields = function() {
        this.firstName.clear();
        this.lastName.clear();
        this.startDate.clear();
        this.email.clear();
  };
};

module.exports = CreatePage;