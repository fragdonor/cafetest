var EditPage = function() {
    this.greeting = element(by.id("greetings"));
    this.deleteButton = element(by.cssContainingText('.main-button', 'Delete'));
    this.logoutButton = element(by.css("body > div > header > div > p.main-button"));
    this.backButton = element(by.css("#sub-nav > li > a"));
    this.updateButton = element(by.buttonText("Update"));
    this.firstName = element(by.model("selectedEmployee.firstName"));
    this.lastName = element(by.model("selectedEmployee.lastName"));
    this.startDate = element(by.model("selectedEmployee.startDate"));
    this.email = element.all(by.model("selectedEmployee.email"));
    this.clearFields = function() {
        browser.sleep(500);
        this.firstName.clear();
        this.lastName.clear();
        this.startDate.clear();
        this.email.clear();
    };
    this.expectUrlEdit = function() {
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.urlContains("edit"), 5000);
    };
    this.deleteEntry=function(){
        this.deleteButton.click().then(function(){
            browser.sleep(100);
        }).then(function(){
            browser.switchTo().alert().accept();
        });
    };
};

module.exports = EditPage;