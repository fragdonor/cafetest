var LoginPage = require("./login_page");
var EmployeesPage = require("./employees_page");
var CreatePage = require("./create_page");
var EditPage = require("./edit_page");
var DeleteFunction = require("./delete_function");

var username = "Luke";
var pw = "Skywalker";
var firstname = "f2irst";
var lastname = "l2ast";
var startdate = "2053-10-10";
var email = "email1@google.com";

describe("Test Cafe login page", function(){
    beforeEach(function() {
        loginPage = new LoginPage();
        employeesPage = new EmployeesPage();
        createPage = new CreatePage();
        editPage = new EditPage();
        deleteFunction = new DeleteFunction();
        browser.get(loginPage.url);
        loginPage.login(username, pw);
        deleteFunction.deleteEntriesFromList(firstname+" "+lastname);
        //deleteFunction.deleteAll();
        browser.sleep(500);
        employeesPage.createButton.click();
        createPage.firstName.sendKeys(firstname);
        createPage.lastName.sendKeys(lastname);
        createPage.startDate.sendKeys(startdate);
        createPage.email.sendKeys(email);
        createPage.addButton.click();
        browser.sleep(500);
        var xpathExp = "//li[contains(.,'"+firstname+' '+lastname+"')]";
        element(by.xpath(xpathExp)).click();
        employeesPage.editButton.click();
    });
    it("To test element data after update was saved", function(){
        browser.sleep(500);
        expect(editPage.firstName.getAttribute('value')).toEqual(firstname);
        expect(editPage.lastName.getAttribute('value')).toEqual(lastname);
        expect(editPage.startDate.getAttribute('value')).toEqual(startdate);
        expect(editPage.email.first().getAttribute('value')).toEqual(email);

    	editPage.clearFields();
        editPage.firstName.sendKeys("first2");
        editPage.lastName.sendKeys("last2");
        editPage.startDate.sendKeys("2052-10-10");
        editPage.email.sendKeys("email2@gmail.com");
        editPage.updateButton.click();
        var xpathExp = "//li[contains(.,'first2 last2')]";
        element(by.xpath(xpathExp)).click();
        browser.sleep(100);
        employeesPage.editButton.click();
        expect(editPage.firstName.getAttribute('value')).toEqual("first2");
        expect(editPage.lastName.getAttribute('value')).toEqual("last2");
        expect(editPage.startDate.getAttribute('value')).toEqual("2052-10-10");
        expect(editPage.email.first().getAttribute('value')).toEqual("email2@gmail.com");
    });
    it("To test greeting", function(){
        expect(editPage.greeting.getText()).toEqual('Hello '+username);
    });
    it("To test logout", function(){
        editPage.logoutButton.click();
        expect(browser.getCurrentUrl()).toEqual(loginPage.url);
        expect(browser.isElementPresent(loginButton)).toEqual(true);
        expect(loginPage.usernameField.getAttribute('value')).toEqual("");
        expect(loginPage.passwordField.getAttribute('value')).toEqual("");
    });
    it("To test back button", function(){
        editPage.backButton.click();
        expect(browser.getCurrentUrl()).toEqual(employeesPage.url);
    });
    it("To test First name required", function(){
        editPage.clearFields();
    	editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Last name required", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Start date required", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Email date required", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test First name with numbers is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys("f1irst");
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test First name with special symbols is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys("first!");
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Last name with numbers is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys("last1");
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test First name with special symbols is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys("last%");
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Date with military type is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys("10102017");
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Date with text is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys("10t1sd017");
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Date with wrong numbers is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys("12345678912345678");
        editPage.email.sendKeys(email);
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Email without @ is not allowed", function(){
        editPage.clearFields();
        editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys("email");
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Email without ending is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys("email@gmail");
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Email with numbers in front is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys("1email@gmail.com");
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Email with wrong ending is not allowed", function(){
        editPage.clearFields();
    	editPage.firstName.sendKeys(firstname);
        editPage.lastName.sendKeys(lastname);
        editPage.startDate.sendKeys(startdate);
        editPage.email.sendKeys("email@gmail.c");
        editPage.updateButton.click();
        editPage.expectUrlEdit();
    });
    it("To test Delete function", function(){
        editPage.deleteEntry();
        expect(browser.getCurrentUrl()).toEqual(employeesPage.url);
        var xpathExp = "//li[contains(.,'"+firstname+' '+lastname+"')]";
        element.all(by.xpath(xpathExp)).then(function(items) {
            expect(items.length).toBe(0);
        });
    });
});